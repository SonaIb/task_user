package com.example.project_2.model;

public enum TaskStatus {
    ACTIVE, TODO, DONE
}
