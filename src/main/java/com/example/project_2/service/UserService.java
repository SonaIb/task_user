package com.example.project_2.service;

import com.example.project_2.model.User;
import com.example.project_2.repository.TaskRepository;
import com.example.project_2.repository.UserRepostirory;
import com.example.project_2.response.UserResponse;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

private final UserRepostirory userRepostirory;

private final TaskRepository taskRepository;

private ModelMapper modelMapper;


    public UserResponse findById(Long id) {
        Optional<User> userOptional = userRepostirory.findById(id);
        if (!userOptional.isPresent()) {
            throw new EntityNotFoundException("Task not found with id: " + id);
        }
        User user = userOptional.get();
        return modelMapper.map(user, UserResponse.class);
    }
}
