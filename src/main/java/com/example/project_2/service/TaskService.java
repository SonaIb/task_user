package com.example.project_2.service;

import com.example.project_2.model.Task;
import com.example.project_2.repository.TaskRepository;
import com.example.project_2.repository.UserRepostirory;
import com.example.project_2.request.CreateTaskRequest;
import com.example.project_2.request.UpdateTaskRequest;
import com.example.project_2.response.TaskResponse;
import com.example.project_2.response.UpdateTaskResponse;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;
    private final ModelMapper modelMapper;

    private final UserRepostirory userRepostirory;


    public TaskResponse create(CreateTaskRequest request){
        Task task = modelMapper.map(request, Task.class);
        Task savedTask = taskRepository.save(task);
        System.out.println(savedTask.getName() + " " + savedTask.getAssignedUsers());
        TaskResponse taskResponse = modelMapper.map(savedTask, TaskResponse.class);
        return taskResponse;
    }

      @Transactional
    public UpdateTaskResponse update(Long id, UpdateTaskRequest newTaskData){

          Task task = taskRepository.findById(id)
                  .orElseThrow(() -> new EntityNotFoundException("Task not found with ID " + id));

          return null;
      }


    public void delete(Long id){
        Optional<Task> task = taskRepository.findById(id);
        if (!task.isPresent()) {
            throw new EntityNotFoundException("Task not found with id: " + id);
        }
        taskRepository.deleteById(id);
    }

    public TaskResponse getById(Long id){
        Optional<Task> taskOptional = taskRepository.findById(id);
        if (!taskOptional.isPresent()) {
            throw new EntityNotFoundException("Task not found with id: " + id);
        }
        Task task = taskOptional.get();
        return modelMapper.map(task, TaskResponse.class);
    }

    public List<TaskResponse> getAll(){
        List<Task> tasks = taskRepository.findAll();
        return tasks.stream().map(task -> modelMapper.map(task, TaskResponse.class)).collect(Collectors.toList());
    }
}

