package com.example.project_2.response;

import com.example.project_2.model.TaskStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.*;
import java.util.Date;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskResponse {

    Long id;
    String name;
    Date deadline;
    @Enumerated(EnumType.STRING)
    TaskStatus status;
    String description;
    List<Long> assignedUserIds = new ArrayList<>();
}
