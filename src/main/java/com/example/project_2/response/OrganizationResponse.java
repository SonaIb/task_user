package com.example.project_2.response;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrganizationResponse {

     Long id;
     String name;
     List<Long> taskIds;
     List<Long> userIds;
}
