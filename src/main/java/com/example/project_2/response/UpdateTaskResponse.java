package com.example.project_2.response;

import com.example.project_2.model.TaskStatus;
import com.example.project_2.model.User;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class UpdateTaskResponse {

    String name;
    @Enumerated(EnumType.STRING)
    TaskStatus status;
    String description;
    Date deadline;
    List<User> assignedUser = new ArrayList<>();
}
