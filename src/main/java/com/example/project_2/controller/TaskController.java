package com.example.project_2.controller;

import com.example.project_2.request.CreateTaskRequest;
import com.example.project_2.request.UpdateTaskRequest;
import com.example.project_2.response.TaskResponse;
import com.example.project_2.response.UpdateTaskResponse;
import com.example.project_2.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {
private final TaskService taskService;
    @PostMapping("/create")
    public ResponseEntity<TaskResponse> createTask(@RequestBody CreateTaskRequest request) {
        TaskResponse taskResponse = taskService.create(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(taskResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskResponse> getById(@PathVariable Long id) {
        TaskResponse taskResponse = taskService.getById(id);
        return ResponseEntity.ok(taskResponse);
    }


    @GetMapping
    public ResponseEntity<List<TaskResponse>> getAll() {
        List<TaskResponse> taskResponses = taskService.getAll();
        return ResponseEntity.ok(taskResponses);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        taskService.delete(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UpdateTaskResponse> update(@PathVariable Long id, @RequestBody UpdateTaskRequest taskRequest) {
        UpdateTaskResponse updatedTask = taskService.update(id, taskRequest);
        return ResponseEntity.ok(updatedTask);
    }



}
