package com.example.project_2.repository;

import com.example.project_2.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepostirory extends JpaRepository<User, Long> {
}
