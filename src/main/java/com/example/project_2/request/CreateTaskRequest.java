package com.example.project_2.request;

import com.example.project_2.model.TaskStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class CreateTaskRequest {

    String name;
    @Enumerated(EnumType.STRING)
    TaskStatus status;
    String description;
    Date deadline;
}
