package com.example.project_2.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequest {

    String name;
    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*[1-9])[A-Za-z1-9]+$")
    String password;
    @Email
    String email;


}
